# Code
face global comment bright-black

# Builtins
face global Prompt                 default,default
face global PrimaryCursor          black,green
face global PrimaryCursorEol       black,green
face global PrimarySelection       default,black
face global SecondaryCursor        black,green
face global SecondaryCursorEol     black,green
face global SecondarySelection     default,black
face global BufferPadding          black,default
face global MenuForeground         bright-black,black
face global MenuBackground         white,black
face global Error                  black,green
