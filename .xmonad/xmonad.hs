--------------------------------------------------------------------------------
-- James's XMonad Config
--------------------------------------------------------------------------------

--
-- Imports
--

-- Base
import XMonad
import qualified Data.Map as M
import Data.Monoid
import qualified XMonad.StackSet as W
import System.Directory

-- Util
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig
import XMonad.Util.Run

-- Actions
import XMonad.Actions.DynamicProjects
import XMonad.Actions.Navigation2D

-- Layout
import XMonad.Layout.Spacing
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.Simplest
import XMonad.Layout.NoBorders

-- Hooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.DynamicBars

-- Prompts
import XMonad.Prompt
import XMonad.Prompt.Shell

--
-- Programs
--

myTerminal :: String
myTerminal = "st"

--
-- Keybinds
--

myModMask :: KeyMask
myModMask = mod4Mask

myKeys = \c -> mkKeymap c $
    -- Action keys
    [ ("M-e", spawn $ terminal c)
    , ("M-w", shellPrompt myPromptConfig)
    , ("M-S-w", xrandrPrompt myPromptConfig "/home/james/bin/randr/")
    , ("M-S-r", spawn "xmonad --recompile && xmonad --restart") 
    , ("M-S-p", spawn "scr")
    , ("M-<Tab>", sendMessage NextLayout)] ++
    -- Media Keys
    [ ("<XF86AudioPlay>", spawn "mpc toggle")
    , ("<XF86AudioStop>", spawn "mpc stop")
    , ("<XF86AudioNext>", spawn "mpc next")
    , ("<XF86AudioPrev>", spawn "mpc prev") ] ++
    -- Go to project keys
    [ ("M-" ++ k, switchProject p)
        | (k, p) <- zip myProjectKeys myProjects ] ++
    -- Shift to project keys
    [ ("M-S-" ++ k, shiftToProject p)
        | (k, p) <- zip myProjectKeys myProjects ] ++
    -- Window Keys
    [ ("M-q", kill)
    , ("M-S-h", sendMessage Shrink)
    , ("M-S-l", sendMessage Expand)
    , ("M-S-j", sendMessage MirrorShrink)
    , ("M-S-k", sendMessage MirrorExpand) 
    , ("M-h", windowGo L False)
    , ("M-l", windowGo R False)
    , ("M-j", windowGo D False)
    , ("M-k", windowGo U False)
    , ("M-C-h", windowSwap L False)
    , ("M-C-l", windowSwap R False)
    , ("M-C-j", windowSwap D False)
    , ("M-C-k", windowSwap U False)
    , ("M1-h", sendMessage $ pushGroup L)
    , ("M1-l", sendMessage $ pushGroup R)
    , ("M1-j", sendMessage $ pushGroup D)
    , ("M1-k", sendMessage $ pushGroup U)
    , ("M1-<Tab>", onGroup W.focusUp' )
    , ("M1-<Space>", withFocused (sendMessage . UnMerge))]

--
-- Per Layout Keys
--

getActiveLayoutDescription :: X String
getActiveLayoutDescription = do
    workspaces <- gets windowset
    return $ description . W.layout . W.workspace . W.current $ workspaces

doIfLayout :: String -> X () -> X ()
doIfLayout l x = do
    layout <- getActiveLayoutDescription
    if layout == l
        then x
        else return ()

layoutKey :: String -> (String, X ()) -> (String, X ())
layoutKey l a = (fst a, doIfLayout l (snd a))

layoutSectionToKey :: String -> [(String, X ())] -> [(String, X ())]
layoutSectionToKey layout actions = map (\action ->
    layoutKey layout action) actions

layoutKeys :: [(String, [(String, X ())])] -> [(String, X ())]
layoutKeys struct = foldr (++) []
    (map (\section ->
        layoutSectionToKey (fst section) (snd section)) struct)

--
-- Theme
--

myBorderWidth :: Dimension
myBorderWidth = 2

myInactiveColor :: String
myInactiveColor = "#24334F"

myActiveColor :: String
myActiveColor = "#1F4B71"

--
-- Startup Hook
--

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "sh ~/.fehbg"
    spawnOnce "setxkbmap gb"
    spawnOnce "setxkbmap -option caps:super"
    spawnOnce "xinput set-prop '/dev/wsmouse' 'WS Pointer Wheel Emulation' 1"
    spawnOnce "xinput set-prop '/dev/wsmouse' 'WS Pointer Wheel Emulation Button' 2"
    spawnOnce "xbanish"
    spawnOnce "xsetroot -cursor_name left_ptr"
    dynStatusBarStartup myDynamicBar myDynamicBarCleanup

-- 
-- Projects
--

myProjectKeys :: [String]
myProjectKeys = ["a", "s", "d", "f", "\\", "z", "x", "c"]

myProjects :: [Project]
myProjects =
    [ Project { projectName = "Home"
              , projectDirectory = "~/"
              , projectStartHook = Nothing } 
    , Project { projectName = "Browser"
              , projectDirectory = "~/"
              , projectStartHook = Nothing }
    , Project { projectName = "Work"
              , projectDirectory = "~/" 
              , projectStartHook = Just $ do
                  spawn myTerminal } 
    , Project { projectName = "Chat"
              , projectDirectory = "~/"
              , projectStartHook = Nothing }
    , Project { projectName = "Music"
              , projectDirectory = "~/media/mus" 
              , projectStartHook = Just $ do
                  spawn (myTerminal ++ " -e ncmpcpp") }
    , Project { projectName = "Server"
              , projectDirectory = "~/" 
              , projectStartHook = Just $ do
                  spawn (myTerminal ++ " -e ssh root@jamespalmer.xyz") } ]

myWorkspaces :: [String]
myWorkspaces = map projectName myProjects

--
-- Layout Hook
--

myLayoutHook = tallgaps ||| fullscreen
    where
        tallgaps = renamed [Replace "Tile"]
            $ avoidStruts $ mySpacing
            $ configurableNavigation noNavigateBorders
            $ subLayout [0] (Simplest)
            $ ResizableTall 1 (1/100) (2/3) []
        fullscreen = renamed [Replace "Full"]
            $ noBorders $ Full
        mySpacing = spacingRaw False (Border 5 5 5 5) True
            (Border 2 2 2 2) True

--
-- Dynamic Bar Spawns
--

-- note: idek why i have to use fromEnum here but it works so ok
-- seems to make it numbers starting from 0 instead of whatever show on a ScreenId Makes
myDynamicBar :: DynamicStatusBar
myDynamicBar screen = spawnPipe ("xmobar -x" ++ (show (fromEnum screen)) ++ " ~/.config/xmobarrc")

myDynamicBarCleanup :: DynamicStatusBarCleanup
myDynamicBarCleanup = return () -- we do nothing, DynamicBars kills the xmobars for us

myBarPP :: PP
myBarPP = xmobarPP
    { ppCurrent = xmobarColor "#CC6331" ""
    , ppVisible = xmobarColor "#607380" ""
    , ppTitle = xmobarStrip
    , ppSep = xmobarColor "#0F4C61" "" " | " }

--
-- Navigation2D Config
--

myNav2DConfig :: Navigation2DConfig
myNav2DConfig = def
    { defaultTiledNavigation = hybridNavigation }

--
-- Prompt Config
--

myPromptConfig :: XPConfig
myPromptConfig = def
    { font = "xft:spleen:pixelsize=16:antialias=false"
    , bgColor = "#010713"
    , fgColor = "#C1D3D6"
    , height = 20
    , promptBorderWidth = 0
    , bgHLight = "#010713"
    , fgHLight = "#CC6331"
    , position = Top
    , showCompletionOnTab = False
    , alwaysHighlight = False
    , maxComplRows = Just 1
}

--
-- XRandR Prompt
--

data XRandR = XRandR
instance XPrompt XRandR where
    showXPrompt XRandR = "Screens: "

xrandrPrompt :: XPConfig -> FilePath -> X ()
xrandrPrompt c p = do
    scripts <- liftIO (listDirectory p)
    mkXPrompt XRandR c
        (xrandrCompleteFunction scripts)
        (\s -> spawn ("sh ~/bin/randr/" ++ s ++ "; sh ~/.fehbg"))

xrandrCompleteFunction :: [String] -> String -> IO [String]
xrandrCompleteFunction l [] = return l
xrandrCompleteFunction l s =
    return $ filter (\x -> take (length s) x == s) l

--
-- Main
--

main :: IO ()
main = xmonad $ ewmh $ docks $ dynamicProjects myProjects
    $ withNavigation2DConfig myNav2DConfig $ def
    { modMask = myModMask
    , terminal = myTerminal
    , borderWidth = myBorderWidth
    , normalBorderColor = myInactiveColor
    , focusedBorderColor = myActiveColor
    , startupHook = myStartupHook
    , workspaces = myWorkspaces
    , manageHook = manageDocks
    , handleEventHook =
        dynStatusBarEventHook myDynamicBar myDynamicBarCleanup <+>
        handleEventHook def <+>
        fullscreenEventHook
    , layoutHook = myLayoutHook
    , keys = myKeys
    , logHook = multiPP myBarPP myBarPP }
